package com.irfan.sampling.androidlatihan15.AllResult

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.irfan.sampling.androidlatihan15.R


/**
 *   created by Irfan Assidiq on 4/19/19
 *   email : assidiq.irfan@gmail.com
 **/
class FragmentResult : Fragment(), ViewResult{

    lateinit var presenter : PresenterResult
    companion object {
        fun newInstance() : FragmentResult{
            return FragmentResult()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter = PresenterResult()
        onAttachView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.result_fragment,
            container, false)
    }

    override fun onShowResult(text: String) {
        var tv_results : TextView =
            activity!!.findViewById(
            R.id.tv_result
        )
        tv_results.setText(text)
    }

    override fun onAttachView() {
        presenter.onAttach(this)
        setResult()
    }

    override fun onDetachView() {
        presenter.onDetach()
    }

    override fun onDestroyView() {
        onDetachView()
        super.onDestroyView()
    }

    fun setResult(){
        val bundle : Bundle? = arguments
        val text = bundle!!.getString("data")
        presenter.showResult(text)
    }
}