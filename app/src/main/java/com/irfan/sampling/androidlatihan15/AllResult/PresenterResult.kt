package com.irfan.sampling.androidlatihan15.AllResult

import com.irfan.sampling.androidlatihan15.INTERFACES.Presenter


/**
 *   created by Irfan Assidiq on 4/19/19
 *   email : assidiq.irfan@gmail.com
 **/
class PresenterResult : Presenter<ViewResult>{

    var mViewResult : ViewResult? = null

    override fun onAttach(views: ViewResult) {
        mViewResult = views
    }

    override fun onDetach() {
        mViewResult = null
    }

    fun showResult(text : String){
        mViewResult?.onShowResult(text)
    }
}