package com.irfan.sampling.androidlatihan15.AllResult

import com.irfan.sampling.androidlatihan15.INTERFACES.Views


/**
 *   created by Irfan Assidiq on 4/19/19
 *   email : assidiq.irfan@gmail.com
 **/
interface ViewResult : Views{
    fun onShowResult(text: String)
}