package com.irfan.sampling.androidlatihan15.INTERFACES


/**
 *   created by Irfan Assidiq on 4/19/19
 *   email : assidiq.irfan@gmail.com
 **/
interface Presenter<T: Views> {
    fun onAttach(views: T)
    fun onDetach()
}