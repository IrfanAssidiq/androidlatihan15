package com.irfan.sampling.androidlatihan15.MAIN

import com.irfan.sampling.androidlatihan15.INTERFACES.Presenter
import com.irfan.sampling.androidlatihan15.MODEL.Data


/**
 *   created by Irfan Assidiq on 4/19/19
 *   email : assidiq.irfan@gmail.com
 **/
class MainPresenter : Presenter<MainView> {

    private var mView : MainView? = null

    override fun onAttach(views: MainView) {
        mView = views
    }

    override fun onDetach() {
        mView = null
    }

    fun showFragment(){
        val data = Data()
        data.text = "Hello From Data!!"

        mView?.onShowFragment(data)
    }

}