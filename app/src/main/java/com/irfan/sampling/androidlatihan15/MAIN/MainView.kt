package com.irfan.sampling.androidlatihan15.MAIN

import com.irfan.sampling.androidlatihan15.INTERFACES.Views
import com.irfan.sampling.androidlatihan15.MODEL.Data


/**
 *   created by Irfan Assidiq on 4/19/19
 *   email : assidiq.irfan@gmail.com
 **/
interface MainView :Views {
    fun onShowFragment(data : Data)
}