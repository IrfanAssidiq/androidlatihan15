package com.irfan.sampling.androidlatihan15.MODEL


/**
 *   created by Irfan Assidiq on 4/19/19
 *   email : assidiq.irfan@gmail.com
 **/
class Data {
    constructor()
    constructor(text : String){
        this.text = text
    }

    var text : String = ""
    get() = field
    set(value) {
        field = value
    }

}